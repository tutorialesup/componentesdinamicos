import { Component, OnInit, ComponentFactoryResolver, ViewChild, Input } from '@angular/core';
import {Comp2Component } from './comp2/comp2.component';
import {ColordinamicoDirective} from './colordinamico.directive';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild(ColordinamicoDirective, {static: true}) eldinamico: ColordinamicoDirective;
  constructor(private cfr: ComponentFactoryResolver) {}
  cbox: boolean;
  ngOnInit() {
    this.cbox = true;
    sessionStorage.setItem('azar', '1');
  }
  cambio(e) {
    this.cbox = e;
    if (e) {
      sessionStorage.setItem('azar', '1');
    } else {
      sessionStorage.setItem('azar', '0');
    }
  }

  componenteDinamico(mensaje: string, color: string) {
    sessionStorage.setItem('mensaje', mensaje);
    sessionStorage.setItem('color', color);
    let cf = this.cfr.resolveComponentFactory(Comp2Component);
    let vcr = this.eldinamico.viewContainerRef;
    vcr.createComponent(cf, 0);
  }
}
