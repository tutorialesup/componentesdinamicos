import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Comp1Component } from './comp1/comp1.component';
import { Comp2Component } from './comp2/comp2.component';
import { Comp3Component } from './comp3/comp3.component';
import { Comp4Component } from './comp4/comp4.component';
import { ReceptorComponent } from './receptor/receptor.component';
import { ReceptorDirective } from './receptor.directive';
import { ColordinamicoDirective } from './colordinamico.directive';

@NgModule({
  declarations: [
    AppComponent,
    Comp1Component,
    Comp2Component,
    Comp3Component,
    Comp4Component,
    ReceptorComponent,
    ReceptorDirective,
    ColordinamicoDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    Comp1Component,
    Comp2Component,
    Comp3Component,
    Comp4Component
  ]
})
export class AppModule { }
