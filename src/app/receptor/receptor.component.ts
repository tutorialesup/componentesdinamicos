import { Component, OnInit, ComponentFactoryResolver, ViewChild, Input } from '@angular/core';
import { Comp1Component } from '../comp1/comp1.component';
import { Comp3Component } from '../comp3/comp3.component';
import { Comp4Component } from '../comp4/comp4.component';
import { ReceptorDirective } from '../receptor.directive';

@Component({
  selector: 'app-receptor',
  templateUrl: './receptor.component.html',
  styleUrls: ['./receptor.component.css']
})
export class ReceptorComponent implements OnInit {
  @ViewChild(ReceptorDirective, {static: true}) receptor: ReceptorDirective;
  @Input() compo: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.agregarCard(this.compo);
  }

  agregarCard(id) {
    let miComponent: any;
    switch (this.compo) {
      case '1':
        miComponent = Comp1Component;
        break;
      case '3':
        miComponent = Comp3Component;
        break;
      case '4':
        miComponent = Comp4Component;
        break;

    }

    if (miComponent) {
      let componentFactory = this.componentFactoryResolver.resolveComponentFactory(miComponent);
      let viewContainerRef = this.receptor.viewContainerRef;
      viewContainerRef.clear();
      viewContainerRef.createComponent(componentFactory);
    }
  }

}
